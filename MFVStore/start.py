import sys
import copy
import os
# Add lib directory to module search path

proj_dir = os.path.abspath(os.path.join(os.getcwd()))
sys.path.append(proj_dir)
lib_dir = os.path.join(proj_dir, 'lib')
sys.path.append(lib_dir)

from MFVStore.Controller.StoreController import StoreController
from datetime import datetime
from datetime import timedelta
from MFVStore.Entities.Owner import Owner
from MFVStore.Entities.Vegetable import Vegetable
from MFVStore.Entities.Fruit import Fruit

if __name__ == '__main__':
    store = StoreController()
    store.running = True

    owner = Owner("OW0001", "testL", "testF", "123456", "owner@monash.edu", 0)
    store.user_list["OW0001"] = owner

    store.user_list.update(store.repo.get_customers_from_repository())

    store.read_data_from_repo()

    if len(store.product_list) == 0:
        initial_discount_percent = 0
        creation_datetime = datetime.utcnow()
        shelf_life_day = 2

        id = store.id_generator.get_product_next_id()
        v1 = Vegetable(id, "Spinach", "local", "fresh produce", "Bunch", 10, 2.5, initial_discount_percent,
                       shelf_life_day, creation_datetime, creation_datetime.date() + timedelta(shelf_life_day))
        store.repo.save_product(v1)

        id = store.id_generator.get_product_next_id()
        f1 = Fruit(id, "Apple", "overseas", "nice apple", "Whole", 10, 3, initial_discount_percent,
                   shelf_life_day, creation_datetime, creation_datetime.date() + timedelta(shelf_life_day))
        store.repo.save_product(f1)

        shelf_life_day = 0
        id = store.id_generator.get_product_next_id()
        v2 = Vegetable(id, "Iceberg", "local", "nice iceberg", "Whole", 3, 2.5, 0,
                       shelf_life_day, creation_datetime, creation_datetime.date() + timedelta(shelf_life_day))
        store.repo.save_product(v2)

        store.read_data_from_repo()

    # temporarily for debug purpose
    #store.user_controller.current_user = owner

    store.product_list = store.product_controller.update_product_list(store.product_list)

    store.ui.display_msg(store.welcome_msg)
    store.redirect_home()

    store.run_store()