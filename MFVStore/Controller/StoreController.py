import sys
import copy
import os
# Add lib directory to module search path

proj_dir = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
sys.path.append(proj_dir)
lib_dir = os.path.join(proj_dir, 'lib')
sys.path.append(lib_dir)

from datetime import datetime
from datetime import timedelta
from MFVStore.UI.UserInterface import UserInterface
from MFVStore.Controller.Repository import Repository
from MFVStore.Entities.Vegetable import Vegetable
from MFVStore.Entities.Fruit import Fruit
from MFVStore.Entities.Customer import Customer
from MFVStore.Entities.Owner import Owner
from MFVStore.Entities.Address import Address
from MFVStore.Entities.IDGenerator import IDGenerator
from MFVStore.Entities.OrderItem import OrderItem
from MFVStore.Entities.Order import Order
from MFVStore.Controller.ProductController import ProductController
from MFVStore.Controller.UserController import UserController
from MFVStore.Controller.OrderController import OrderController


class StoreController:

    def __init__(self):

        self.__running = False
        self.__user_list = dict()
        self.__product_list = dict()
        self.__order_item_list = dict()
        self.__order_list = dict()
        self.__ui = UserInterface()
        self.__repo = Repository()
        self.__id_generator = IDGenerator()
        self.__welcome_msg = "Welcome the MFV Store!"
        self.__product_controller = ProductController()
        self.__user_controller = UserController()
        self.__order_controller = OrderController()
        self.__page = "start_menu"    # initialize the first page

        # Setup configurable menu for display in User Interface
        self.__menu_list = {
            "start_menu": {
                "Login": {"Description": "Login", "Function": self.to_login},
                "Register": {"Description": "Register", "Function": self.to_register},
                "Search product": {"Description": "Search product", "Function": self.to_search_product},
                "Exit": {"Description": "Exit", "Function": self.to_exit}
            },

            # For Owner
            "owner_home_menu": {
                "Search product": {"Description": "Search product", "Function": self.to_search_product},
                "Add product": {"Description": "Add product", "Function": self.to_add_product},
                "View customers": {"Description": "View customers", "Function": self.to_view_customer_list},
                "Logout": {"Description": "Logout", "Function": self.to_logout},
                "Exit": {"Description": "Exit", "Function": self.to_exit},
                "Input number": {"Description": "Select product", "Function": self.to_view_product}
            },
            "owner_review_product_menu": {
                "Edit product": {"Description": "Edit product", "Function": self.to_edit_product},
                "Remove product": {"Description": "Remove product", "Function": self.to_remove_product},
                "Go back": {"Description": "Go back home page", "Function": self.redirect_home},
                "Logout": {"Description": "Logout", "Function": self.to_logout},
                "Exit": {"Description": "Exit", "Function": self.to_exit}
            },
            "owner_view_customer_list_menu": {
                "Go back": {"Description": "Go back home page", "Function": self.redirect_home},
                "Logout": {"Description": "Logout", "Function": self.to_logout},
                "Exit": {"Description": "Exit", "Function": self.to_exit},
                "Input number": {"Description": "Select customer to show detail", "Function": self.to_view_customer}
            },
            "owner_view_customer_menu": {
                "View order history": {"Description": "View order history", "Function": self.to_view_order_history},
                "View cart": {"Description": "View customer's shopping cart", "Function": self.to_view_customer_cart},
                "Remove customer": {"Description": "Remove customer", "Function": self.to_remove_customer},
                "Go back": {"Description": "Go back home page", "Function": self.redirect_home},
                "Logout": {"Description": "Logout", "Function": self.to_logout},
                "Exit": {"Description": "Exit", "Function": self.to_exit}
            },
            "owner_review_order_history": {
                "View cart": {"Description": "View customer's shopping cart", "Function": self.to_view_customer_cart},
                "Remove customer": {"Description": "Remove customer", "Function": self.to_remove_customer},
                "Go back": {"Description": "Go back home page", "Function": self.redirect_home},
                "Logout": {"Description": "Logout", "Function": self.to_logout},
                "Exit": {"Description": "Exit", "Function": self.to_exit}
            },

            # For Customers
            "customer_home_menu": {
                "Search product": {"Description": "Search product", "Function": self.to_search_product},
                "View account": {"Description": "View my account detail", "Function": self.to_view_account},
                "View Cart": {"Description": "View Cart", "Function": self.to_view_cart},
                "View Orders": {"Description": "View Orders", "Function": self.to_view_orders},
                "Logout": {"Description": "Logout", "Function": self.to_logout},
                "Exit": {"Description": "Exit", "Function": self.to_exit},
                "Input number": {"Description": "Select product to add to cart", "Function": self.to_add_to_cart}
            },
            "customer_review_account_menu": {
                "Edit account": {"Description": "Edit detail", "Function": self.to_edit_account},
                "Recharge": {"Description": "Recharge", "Function": self.to_recharge},
                "Unregister": {"Description": "Unregister account", "Function": self.to_unregister},
                "Redirect home": {"Description": "Redirect home", "Function": self.redirect_home},
                "Logout": {"Description": "Logout", "Function": self.to_logout}
            },
            "customer_purchase_menu": {
                "Purchase": {"Description": "Purchase", "Function": self.to_purchase},
                "View Cart": {"Description": "View Cart", "Function": self.to_view_cart},
                "Go back": {"Description": "Go back home page", "Function": self.redirect_home},
                "Logout": {"Description": "Logout", "Function": self.to_logout},
                "Exit": {"Description": "Exit", "Function": self.to_exit},
                "Input number": {"Description": "Select product to add to cart", "Function": self.to_add_to_cart}
            },
            "customer_view_cart_menu": {
                "Check out": {"Description": "Check out", "Function": self.to_purchase},
                "Add more products": {"Description": "Add more products", "Function": self.to_add_more_products},
                "Go back": {"Description": "Go back home page", "Function": self.redirect_home},
                "Logout": {"Description": "Logout", "Function": self.to_logout},
                "Exit": {"Description": "Exit", "Function": self.to_exit},
                "Input number": {"Description": "Edit Order Item Quantity", "Function": self.to_edit_quantity}
            },
            "go_back_menu": {
                "Go back": {"Description": "Go back home page", "Function": self.redirect_home},
                "Logout": {"Description": "Logout", "Function": self.to_logout},
                "Exit": {"Description": "Exit", "Function": self.to_exit}
            },
            "insufficient_balance_menu": {
                "Recharge": {"Description": "Recharge", "Function": self.to_recharge},
                "View Cart to Edit Quantity": {"Description": "View Cart to Edit Quantity", "Function": self.to_view_cart},
                "Go back": {"Description": "Go back home page", "Function": self.redirect_home},
                "Logout": {"Description": "Logout", "Function": self.to_logout},
                "Exit": {"Description": "Exit", "Function": self.to_exit}
            }
        }

    # driver function to run the app
    def run_store(self):
        while self.running:
            self.display()

            # utilize the configurable menu to determine the next step
            try:
                next_step_option = self.__ui.get_user_option()
                if self.__id_generator.is_id(next_step_option):
                    self.__menu_list[self.__page]["Input number"]["Function"](next_step_option)
                else:
                    self.__menu_list[self.__page][next_step_option]["Function"]()

            except KeyError as e:
                self.__ui.display_msg("Invalid selection")
                continue

            except AttributeError as e:
                self.__ui.display_msg(str(e))
                continue

            except ValueError as e:
                self.__ui.display_msg(str(e))
                continue

            except IOError as e:
                self.__ui.display_msg(str(e))
                self.to_exit()

            except KeyboardInterrupt:
                self.to_exit()

    def to_login(self):
        # Function that gets credential from User controller and logs in
        if self.__running:
            # user_credential[0] for user_name, user_credential[1] for password
            user_credential = self.__user_controller.get_user_credential(self.__ui)

            if user_credential is not None:
                self.__user_controller.login(self.__user_list, self.__ui, user_credential)

            self.redirect_home()

    def to_logout(self):
        # Function that logs out the user
        if self.__running:
            self.__user_controller.logout()
            self.redirect_home()

    def to_register(self):
        # Function that calls the User Controller to register a new user
        if self.__running:
            self.__user_controller.register(self.__ui, self.__id_generator)
            self.__user_list[self.__user_controller.current_user.id] = self.__user_controller.current_user
            self.__repo.add_customer(self.__user_list[self.__user_controller.current_user.id])
            self.__ui.display_msg("\n You have been successfully registered. Your user id is " +
                                  str(self.__user_controller.current_user.user_id))
            self.page = "customer_review_account_menu"

    def to_unregister(self):
        # Function that calls User Controller's unregister function
        if self.__running:
            self.__user_controller.unregister(self.__repo, self.__ui, self.__user_list)
            self.redirect_home()

    def to_edit_account(self, user_id=None):
        # Function to edit a user's account details
        if self.__running:
            if user_id is not None:
                self.__user_controller.selected_user = self.__user_list[user_id]

            self.__user_controller.edit_account_from_ui(self.__ui)
            self.__page = "customer_review_account_menu"

    def to_exit(self):
        if self.__running:
            sys.exit(0)

    def to_search_product(self):
        # Function that interacts with Product Controller to search a particular product
        if self.running:
            self.__product_controller.search_product(self.__product_list, self.__ui)

    def to_view_product(self, product_id=None):
        # Function that interacts with Product Controller to view a selected product
        if self.__running:
            if product_id is not None:
                self.__product_controller.selected_product = self.__product_list[product_id]

            self.__product_controller.display_product_detail(self.__ui)
            self.__page = "owner_review_product_menu"

    def to_edit_product(self, product_id=None):
        # Function that interacts with Product Controller to edit a product's details (only by owner)
        if self.__running:
            if product_id is not None:
                self.__product_controller.selected_product = self.__product_list[product_id]

            self.__product_controller.edit_product_from_ui(self.__repo, self.__ui, self.__product_list)
            self.__page = "owner_review_product_menu"

    def to_remove_product(self, product_id=None):
        # Function that interacts with Product Controller to delete a product(Owner)
        if self.__running:
            if product_id is not None:
                self.__product_controller.selected_product = self.__product_list[product_id]

            self.__product_controller.remove_product(self.__repo, self.__ui, self.__product_list)
            self.redirect_home()

    def to_add_product(self):
        # Function that interacts with Product Controller to add a new product to repo(Owner)
        if self.__running:
            if self.__user_controller.current_user is None or \
                    isinstance(self.__user_controller.current_user, Owner) is False:
                raise AttributeError("No valid login owner")

            self.__product_controller.add_new_product_from_ui(
                self.__repo, self.__ui, self.__product_list, self.__id_generator
            )
            self.__page = "owner_review_product_menu"

    def to_add_to_cart(self, product_id):
        # # Function that interacts with Product, User and Order controllers to add items to cart
        if self.__running:
            if self.__user_controller.current_user is None or \
                    isinstance(self.__user_controller.current_user, Customer) is False:
                raise AttributeError("No valid login customer")

            self.__product_controller.get_selected_product(self.__product_list, product_id)
            self.__order_controller.add_to_cart(
                self.__user_controller.current_user,
                self.__product_controller.selected_product,
                self.__ui,
                self.__id_generator,
                self.__repo
            )
            self.__page = "customer_purchase_menu"
            self.__product_controller.display_valid_products(self.__ui, self.__product_list)

    def to_view_customer_list(self):
        # Function that displays all customers
        if self.__running:
            if self.__user_controller.current_user is None or \
                    isinstance(self.__user_controller.current_user, Owner) is False:
                raise AttributeError("No valid login owner")

            self.__user_controller.display_customers(self.__ui, self.__user_list)
            self.__page = "owner_view_customer_list_menu"

    def to_view_customer(self, user_id=None):
        # Function to view details of a particular customer
        if self.__running:
            if self.__user_controller.current_user is None or \
                    isinstance(self.__user_controller.current_user, Owner) is False:
                raise AttributeError("No valid login owner")
            if user_id is not None:
                self.__user_controller.selected_user = self.__user_list[user_id]

            self.__user_controller.display_account_detail(self.__ui)
            self.__page = "owner_view_customer_menu"

    def to_view_order_history(self):
        # Function to view order history of a user
        if self.__running:
            if self.__user_controller.current_user is None or \
                    isinstance(self.__user_controller.current_user, Owner) is False:
                raise AttributeError("No valid login owner")

            self.__order_controller.view_orders(self.__user_controller.selected_user, self.__ui)
            self.__page = "owner_review_order_history"

    def to_view_customer_cart(self):
        # Function to view the cart of a user
        if self.__running:
            if self.__user_controller.current_user is None or \
                    isinstance(self.__user_controller.current_user, Owner) is False:
                raise AttributeError("No valid login owner")

            self.__order_controller.view_cart(self.__user_controller.selected_user, self.__ui)
            self.__page = "owner_view_customer_menu"

    def to_remove_customer(self):
        # Function that interacts with User Controller to remove a customer
        if self.__running:
            if self.__user_controller.current_user is None or \
                    isinstance(self.__user_controller.current_user, Owner) is False:
                raise AttributeError("No valid login owner")

            self.__user_controller.remove_customer(self.__repo, self.__ui, self.__user_list)
            self.__user_controller.display_customers(self.__ui, self.__user_list)
            self.__page = "owner_view_customer_list_menu"

    def to_view_account(self):
        if self.__running:
            if self.__user_controller.current_user is None or \
                    isinstance(self.__user_controller.current_user, Customer) is False:
                raise AttributeError("No valid login user")

            self.__user_controller.selected_user = self.__user_controller.current_user
            self.__user_controller.display_account_detail(self.__ui)
            self.__page = "customer_review_account_menu"

    def to_purchase(self):
        # Function that handles purchase product scenario by interacting with User controller
        if self.running:
            if self.__user_controller.current_user is None or \
                    isinstance(self.__user_controller.current_user, Customer) is False:
                raise AttributeError("No valid login customer")

            message = self.__order_controller.purchase(
                self.__ui, self.__user_controller.current_user, self.product_list, self.__id_generator, self.__repo,
                extra_columns=["product_name"]
            )

            if message is None:
                self.__page = "go_back_menu"
            elif message == "balance":
                self.__ui.display_msg("Insufficient balance to complete an order.\nYour current balance is " +
                                      str(self.__user_controller.current_user.balance))
                self.__page = "insufficient_balance_menu"
            else:
                self.__ui.display_msg(message)
                self.__ui.display_msg("Re-add item into cart or proceed with purchase")
                self.redirect_home()

    def to_recharge(self):
        # Function that recharger the account balance of a user
        if self.running:
            if self.__user_controller.current_user is None or \
                    isinstance(self.__user_controller.current_user, Customer) is False:
                raise AttributeError("No valid login customer")

            self.__user_controller.recharge(self.__ui)
            self.__ui.display_msg("Your balance after recharge: " +
                                  str(self.__user_controller.current_user.balance))
            self.redirect_home()

    def to_view_cart(self):
        if self.running:
            if self.__user_controller.current_user is None or \
                    isinstance(self.__user_controller.current_user, Customer) is False:
                raise AttributeError("No valid login customer")

            self.__order_controller.view_cart(self.__user_controller.current_user, self.__ui,
                                              product_list=self.__product_list, extra_columns=["product_name"])

            self.__page = "customer_view_cart_menu"

    def to_view_orders(self):
        if self.running:
            if self.__user_controller.current_user is None or \
                    isinstance(self.__user_controller.current_user, Customer) is False:
                raise AttributeError("No valid login customer")

            self.__order_controller.view_orders(self.__user_controller.current_user, self.__ui,
                                                product_list=self.__product_list, extra_columns=["product_name"])
            self.__page = "go_back_menu"

    def to_add_more_products(self):
        self.redirect_home()

    def to_edit_quantity(self, order_item_id):
        # Function used to edit quantity in the cart
        if self.__running:
            if self.__user_controller.current_user is None or \
                    isinstance(self.__user_controller.current_user, Customer) is False:
                raise AttributeError("No valid login customer")

            self.__order_controller.edit_order_item_quantity(order_item_id, self.__user_controller.current_user,
                                                             self.__product_list, self.__ui)
            self.__page = "customer_view_cart_menu"
            self.__order_controller.view_cart(self.__user_controller.current_user, self.__ui)

    def read_data_from_repo(self):
        # Function called to read persisted JSON files data
        self.__product_list = self.__repo.get_products_from_repository()

    def redirect_home(self):
        append_table = False
        if self.__user_controller.current_user is None:
            self.__page = "start_menu"
        elif isinstance(self.user_controller.current_user, Owner):
            append_table = True
            self.__product_controller.display_expired_product(self.__ui, self.__product_list)
            self.__page = "owner_home_menu"
        else:
            self.__page = "customer_home_menu"
        self.__product_controller.display_valid_products(self.__ui, self.__product_list, to_append_table=append_table)

    def display(self):
        menu = self.__menu_list[self.__page]
        display_menu = copy.deepcopy(menu)

        self.__ui.menu = [dict({k: {"Description": v["Description"]}}) for k, v in display_menu.items()]
        self.__ui.display()

    @property
    def running(self):
        return self.__running

    @running.setter
    def running(self, running):
        self.__running = running

    @property
    def product_list(self):
        return self.__product_list

    @product_list.setter
    def product_list(self, product_list):
        self.__product_list = product_list

    @property
    def ui(self):
        return self.__ui

    @property
    def repo(self):
        return self.__repo

    @property
    def menu_list(self):
        return self.__menu_list

    @property
    def user_list(self):
        return self.__user_list

    @user_list.setter
    def user_list(self, user_list):
        self.__user_list = user_list

    @property
    def id_generator(self):
        return self.__id_generator

    @property
    def welcome_msg(self):
        return self.__welcome_msg

    @property
    def product_controller(self):
        return self.__product_controller

    @property
    def user_controller(self):
        return self.__user_controller

    @property
    def order_controller(self):
        return self.__order_controller

    @property
    def page(self):
        return self.__page

    @page.setter
    def page(self, page):
        self.__page = page


# if __name__ == '__main__':
#     store = StoreController()
#     store.running = True
#
#     owner = Owner("OW0001", "testL", "testF", "1234", "testO@monash.edu", 0)
#     store.user_list["OW0001"] = owner
#
#     store.user_list.update(store.repo.get_customers_from_repository())
#
#     store.read_data_from_repo()
#
#     if len(store.product_list) == 0:
#         initial_discount_percent = 0
#         creation_datetime = datetime.utcnow()
#         shelf_life_day = 2
#         v1 = Vegetable("P0001", "Spinach", "local", "fresh produce", "Bunch", 10, 2.5, initial_discount_percent,
#                        shelf_life_day, creation_datetime, creation_datetime.date() + timedelta(shelf_life_day))
#         store.repo.add_product(v1)
#
#         f1 = Fruit("P0002", "Apple", "overseas", "nice apple", "Whole", 0, 3, initial_discount_percent,
#                    shelf_life_day, creation_datetime, creation_datetime.date() + timedelta(shelf_life_day))
#         store.repo.add_product(f1)
#
#         shelf_life_day = 0
#         v2 = Vegetable("P0003", "Iceberg", "local", "nice iceberg", "Whole", 3, 2.5, 0,
#                        shelf_life_day, creation_datetime, creation_datetime.date() + timedelta(shelf_life_day))
#         store.repo.add_product(v2)
#
#         store.read_data_from_repo()
#
#     # temporarily for debug purpose
#     #store.user_controller.current_user = owner
#
#     store.product_list = store.product_controller.update_product_list(store.product_list)
#
#     store.ui.display_msg(store.welcome_msg)
#     store.redirect_home()
#
#     store.run_store()
