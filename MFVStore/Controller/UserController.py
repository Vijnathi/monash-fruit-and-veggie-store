import copy
from MFVStore.Entities.Address import Address
from MFVStore.Entities.Customer import Customer
from MFVStore.Entities.ShoppingCart import ShoppingCart
from MFVStore.Entities.User import User


class UserController:

    display_attributes = [
        "ID", "Last name", "First_name", "Email", "Address", "Phone number", "Balance"
    ]

    def __init__(self):
        self.__current_user = None
        self.__selected_user = None
        
    @property
    def selected_user(self):
        return self.__selected_user
    
    @selected_user.setter
    def selected_user(self, selected_user):
        self.__selected_user = selected_user
        
    @property
    def current_user(self):
        return self.__current_user

    @current_user.setter
    def current_user(self, current_user):
        self.__current_user = current_user

    def login(self, user_list, ui, user_credential):
        self.__current_user = self.get_valid_user(user_list, ui, user_credential)

    def logout(self):
        self.__current_user = None

    def get_user_credential(self, ui):
        user_name = ui.get_user_name()
        user_password = ui.get_password_login()
        return (user_name, user_password)

    def get_valid_user(self, user_list, ui, user_credential):
        # user_credential[0] for login_user_id, user_credential[1] for password
        while True:
            login_user_id = user_credential[0]
            password = user_credential[1]
            for id in user_list:
                user = user_list[id]
                if user.user_id == login_user_id:
                    if self.user_suspended(user):
                        raise AttributeError("Account suspended")
                    if user.password == password:
                        return user
                    else:
                        user.fail_login_no += 1
            raise AttributeError("Incorrect User Id or Password")

    def user_suspended(self, user):
        if user.fail_login_no < 5:
            return False
        else:
            return True

    def register(self, ui, id_generator):
        # Get new customer details from UI and register to system
        new_customer = self.get_customer_from_ui(ui, id_generator)
        self.__current_user = new_customer
        self.__selected_user = new_customer
        self.display_account_detail(ui)

    def unregister(self, repo, ui, user_list):
        # Unregister a particular user from the system
        self.__selected_user = self.__current_user
        if self.__selected_user is not None and isinstance(self.__selected_user, Customer):
            customer_id = self.__selected_user.id
            self.__selected_user = None
            self.__current_user = None
            del user_list[customer_id]
            # repo.delete_customer(customer_id)
        else:
            raise AttributeError("Error in unregistering")
        ui.display_msg("Successfully unregister, and logged out!\n")

    def display_customers(self, ui, user_list):
        customer_list = dict((k, v) for k, v in user_list.items() if isinstance(v, Customer))
        ui.display_table("customers", customer_list, self.display_attributes)

    def display_account_detail(self, ui):
        ui.display_table_row(self.__selected_user, self.display_attributes)

    def get_customer_from_ui(self, ui, id_generator):
        # Get new customer details from UI
        last_name = ui.get_last_name()
        first_name = ui.get_first_name()
        email = ui.get_email()
        password = ui.get_password()
        fail_login_no = 0

        street_no = ui.get_street_number()
        street_addr = ui.get_street()
        city = ui.get_city()
        country = ui.get_country()
        postcode = ui.get_postcode()
        address = Address(street_no, street_addr, city, country, postcode)

        phone_number = ui.get_phone_number()

        shopping_cart = ShoppingCart(dict())
        order_history = dict()
        balance = 100
        customer_id = id_generator.get_customer_next_id()

        return Customer(customer_id, last_name, first_name, password, fail_login_no, email,
                        address, shopping_cart, order_history, phone_number, balance)

    def edit_account_from_ui(self, ui):
        # Edit attributes of the customer based on user input
        if self.__selected_user is not None and isinstance(self.__selected_user, User):
            editable_attributes = {
                "last_name": {"Description": "Last name", "Function": self.update_last_name, "Type": str},
                "first_name": {"Description": "First name", "Function": self.update_first_name, "Type": str},
                "password": {"Description": "Password", "Function": self.update_password, "Type": str},
                "email": {"Description": "Email", "Function": self.update_email, "Type": str},
                "address": {"Description": "Address", "Function": self.get_address_from_ui},
                "phone_number": {"Description": "Phone number", "Function": self.update_phone_number, "Type": str},
                "return": {"Description": "Return/Done editing"}
            }

            while True:
                ui.display([dict({k: {"Description": v["Description"]}}) for k, v in editable_attributes.items()])

                option = ui.get_user_option()
                if option == "return":
                    self.display_account_detail(ui)
                    break
                if option == "address":
                    editable_attributes[option]["Function"](ui)
                else:
                    new_value_str = ui.get_new_value(editable_attributes[option].get("Description"))
                    new_value = editable_attributes[option]["Type"](new_value_str)
                    editable_attributes[option]["Function"](new_value)
                ui.display_msg(editable_attributes[option].get("Description") + " is updated!")

        else:
            raise AttributeError("No selected account")

    def get_address_from_ui(self, ui):
        # Get new address from UI
        if self.__selected_user is not None and isinstance(self.__selected_user, User):
            editable_attributes = {
                "street_number": {"Description": "Street Number", "Function": self.update_street_number, "Type": str},
                "street": {"Description": "Street", "Function": self.update_street, "Type": str},
                "city": {"Description": "City", "Function": self.update_city, "Type": str},
                "country": {"Description": "Country", "Function": self.update_country, "Type": str},
                "postcode": {"Description": "Postcode", "Function": self.update_postcode, "Type": str},
                "return": {"Description": "Return/Done editing"}
            }

            while True:
                ui.display([dict({k: {"Description": v["Description"]}}) for k, v in editable_attributes.items()])

                option = ui.get_user_option()
                if option == "return":
                    return
                    break
                else:
                    new_value_str = ui.get_new_value(editable_attributes[option].get("Description"))

                new_value = editable_attributes[option]["Type"](new_value_str)
                editable_attributes[option]["Function"](new_value)
                ui.display_msg(editable_attributes[option].get("Description") + " is updated!")

        else:
            raise AttributeError("No selected account")
        # editable_attributes

    def remove_customer(self, repo, ui, user_list):
        # Delete a customer from the system(OWNER)
        if self.__selected_user is not None and isinstance(self.__selected_user, Customer):
            customer_id = self.__selected_user.id
            del user_list[customer_id]
            repo.delete_customer(self.__selected_user.id)
        else:
            raise AttributeError("Selected customer is not valid")
        ui.display_msg("Successfully deleted the customer")

    def recharge(self, ui):
        # Recharge customer's balance
        amount = ui.get_amount()
        self.__current_user.balance += amount

    def update_last_name(self, new_value):
        self.__selected_user.last_name = new_value

    def update_first_name(self, new_value):
        self.__selected_user.first_name = new_value

    def update_password(self, new_value):
        self.__selected_user.password = new_value

    def update_email(self, new_value):
        self.__selected_user.email = new_value

    def update_address(self, new_value):
        self.__selected_user.address = new_value

    def update_phone_number(self, new_value):
        self.__selected_user.phone_number = new_value

    def update_street_number(self, new_value):
        self.__selected_user.address.street_number = new_value

    def update_street(self, new_value):
        self.__selected_user.address.street_address = new_value

    def update_city(self, new_value):
        self.__selected_user.address.city = new_value

    def update_country(self, new_value):
        self.__selected_user.address.country = new_value

    def update_postcode(self, new_value):
        self.__selected_user.address.postcode = new_value