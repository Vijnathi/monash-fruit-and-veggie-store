
import json
import os
from pathlib import Path
from datetime import datetime
from datetime import timedelta

from MFVStore.Entities.Address import Address
from MFVStore.Entities.Customer import Customer
from MFVStore.Entities.Fruit import Fruit
from MFVStore.Entities.Vegetable import Vegetable
import datetime

from MFVStore.Entities.Order import Order
from MFVStore.Entities.OrderItem import OrderItem
from MFVStore.Entities.Product import Product
from MFVStore.Entities.ShoppingCart import ShoppingCart


class Repository:
    def __init__(self):
        self.datadir = "Data"
        self.product_list_filename = self.get_data_directory() / "ProductList.json"
        self.customer_list_filename = self.get_data_directory() / "CustomerList.json"
        self.initialise_files()

    def initialise_files(self):
        # Touch files for initialisation
        open(self.product_list_filename,'a').close()
        open(self.customer_list_filename,'a').close()

    def write_json(self, dictionary, filename, mode):
        # Write JSON to file in a mode
        with open(filename, mode) as outfile:
            json.dump(dictionary, outfile)

    def read_json(self, filename):
        # Read data from file into dictionary
        with open(filename) as infile:
            try:
                data = json.load(infile)
            except ValueError:
                data = {}
            return data

    def get_data_directory(self):
        current_dir = Path(os.getcwd())
        working_dir = current_dir / self.datadir
        if not os.path.exists(working_dir):
            os.makedirs(working_dir)
        return working_dir

    def delete_product(self, pid):
        # Load data from file, and delete the given product
        with open(self.product_list_filename) as infile:
            data = json.load(infile)
            if pid in data.keys():
                del data[pid]
                self.write_json(data, self.product_list_filename, 'w')

            else:
                print("Product with PID " + pid + "not found ")

    def save_product(self, product):
        # Add product to file repository
        data = self.read_json(self.product_list_filename)
        data[product.product_id] = product.as_dict()
        self.write_json(data, self.product_list_filename, 'w')

    def add_customer(self, customer):
        # Coding only for customers, not generic to user (As we have only one owner)
        data = self.read_json(self.customer_list_filename)
        data[customer.id] = customer.as_dict()
        self.write_json(data, self.customer_list_filename, 'w')

    def delete_customer(self, cid):
        # Delete customer from repository
        with open(self.customer_list_filename) as infile:
            data = json.load(infile)
            if cid in data.keys():
                del data[cid]
                self.write_json(data, self.customer_list_filename, 'w')

            else:
                print("Person with CID " + cid + "not found ")

    def get_products_from_repository(self):
        # Load all the products and return as a dictionary
        file_data = self.read_json(self.product_list_filename)
        final_data = {}
        for key, val in file_data.items():
            if val['product_type'] == "fruit":
                p = Fruit(val['product_id'], val['product_name'], val['product_source'],
                          val['description'], val['package_type'], val['stock_quantity'],
                          val['package_price'], val['discount_percentage'], val['shelf_life_day'],
                          datetime.datetime.strptime(val['creation_datetime'], '%Y-%m-%d %H:%M:%S.%f'),
                          datetime.datetime.strptime(val['expiry'], '%Y-%m-%d').date())
                final_data[key] = p
            elif val['product_type'] == "vegetable":
                p = Vegetable(val['product_id'], val['product_name'], val['product_source'],
                              val['description'], val['package_type'], val['stock_quantity'],
                              val['package_price'], val['discount_percentage'], val['shelf_life_day'],
                              datetime.datetime.strptime(val['creation_datetime'], '%Y-%m-%d %H:%M:%S.%f'),
                              datetime.datetime.strptime(val['expiry'], '%Y-%m-%d').date())
                final_data[key] = p
            else:
                pass
        return final_data

    def get_customers_from_repository(self):
        # Load all the customers from file and return a dictionary
        file_data = self.read_json(self.customer_list_filename)
        final_data = {}
        for key, val in file_data.items():
            order_item_list = val['shopping_cart']['ordered_item_list']
            order_items = {}
            for k, v in order_item_list.items():
                order_items[k] = OrderItem(v['order_item_id'],v['product_id'],v['package_type'],v['quantity'],v['price'])
            cart = ShoppingCart(order_items)

            address_dict = val['address']
            address = Address(address_dict['street_number'], address_dict['street_address'], address_dict['city'],
                              address_dict['country'], address_dict['postcode'])

            orders_dict = val['order_history']
            orders = {}
            order_history_items = {}
            for k, v in orders_dict.items():
                order_item_list_new = v['ordered_item_list']
                for l, value1 in order_item_list_new.items():
                    order_history_items[l] = OrderItem(value1['order_item_id'],value1['product_id'],\
                                                         value1['package_type'],value1['quantity'],value1['price'])

                address_dict = v['delivery_address']
                address = Address(
                    address_dict['street_number'], address_dict['street_address'], address_dict['city'],
                    address_dict['country'],address_dict['postcode']
                )

                orders[k] = Order(
                    v['order_id'], datetime.datetime.strptime(v['transact_time'], '%Y-%m-%d %H:%M:%S.%f'),
                    order_history_items, address, v['total_amount']
                )

            customer = Customer(val['id'], val['last_name'], val['first_name'], val['password'],
                                val['fail_login_no'], val['email'], address, cart, orders,
                                val['phone_number'], val['balance'])
            final_data[key] = customer
        return final_data


# if __name__ == '__main__':
#
#
#     now = datetime.datetime.now()
#     f1 = Fruit('p001','Apple','Farm','desc','type', 5, 10, 50, 5, str(now),str(now))
#     f2= Fruit('p002','Grapes','Farm','desc','type', 5, 10, 50, 5, str(now),str(now))
#     f3 = Fruit('p003','Banana','Farm','desc','type', 5, 10, 50, 5, str(now),str(now))
#
#     r = Repository()
#     r.add_product(f1)
#     r.add_product(f2)
#     new_product_list = r.get_products_from_repository()
#     # print(newproductlist)
#
#     new_product_list[f3.product_id] = f3.as_dict()
#     r.add_product(f3)
#     #r.delete_product(f2.product_id)
#     final_list = r.get_products_from_repository()
#     print(final_list)
#
#     odit = OrderItem('o001',2,"Bag",2,100)
#     odit2 = OrderItem('o002',2,"Bunch",2,100)
#     odit_dict = dict()
#     odit_dict[odit.order_item_id] = odit
#     odit_dict[odit2.order_item_id] = odit2
#     cart = ShoppingCart(odit_dict)
#     orders = {}
#     orders['or001'] = Order('or001',str(now),odit_dict,"melb",1000)
#     orders['or002'] = Order('or002',str(now),odit_dict,"melb",1000)
#     address = Address("2","jasperrod","melbourne","aus","3204")
#     adi = Customer("c1","Adi","bhat","pwd",1,"abc@xyz.com",address,cart,orders,"234",100)
#
#     #newcustlist = r.get_customers_from_repository()
#     #r.add_customer(adi)
#     newcustlist = r.get_customers_from_repository()
#     print(newcustlist)
