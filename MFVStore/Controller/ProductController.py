from datetime import timedelta
from datetime import datetime
from copy import deepcopy
from MFVStore.Entities.Vegetable import Vegetable
from MFVStore.Entities.Fruit import Fruit
from MFVStore.Entities.Product import Product


class ProductController():

    display_attributes = [
        "Product ID", "Product name", "Product source", "Description", "Package type",
        "Stock quantity", "Original price", "Current price", "Discount percentage", "Expiry"
    ]

    product_type_option = {
        "Fruit": {"Description": "Fruit"},
        "Vegetable": {"Description": "Vegetable"}
    }

    package_type_option = {
        "By weight": {"Description": "By weight (kg)"},
        "By bag": {"Description": "By bag"},
        "By bunch": {"Description": "By bunch"},
        "One whole": {"Description": "One whole product"},
        "Whole or half": {"Description": "One whole product or half product"}
    }

    product_source_option = {
        "Overseas": {"Description": "Overseas"},
        "Local": {"Description": "Local"}
    }

    discount_start_day = 1
    discount_percentage = 0.5

    def __init__(self):
        self.__selected_product = None

    @property
    def selected_product(self):
        return self.__selected_product

    @selected_product.setter
    def selected_product(self, product):
        self.__selected_product = product

    def display_valid_products(self, ui, product_list, to_append_table=False):
        # show products that is not expired (expiry date > current date) and has quantity larger than zero
        product_list = self.update_product_list(product_list)
        valid_product_list = {
            product_id: product for product_id, product in product_list.items()
            if product.expiry > datetime.utcnow().date()
        }
        self.display_products(ui, valid_product_list, to_append_table=to_append_table)

    def display_expired_product(self, ui, product_list, to_append_table=False):
        # Display expired products
        product_list = self.update_product_list(product_list)
        expired_product_list = {
            product_id: product for product_id, product in product_list.items()
            if product.expiry <= datetime.utcnow().date()
        }
        self.display_products(
            ui, expired_product_list, msg="expired products (Kindly move them to charity or dispose)", to_append_table=to_append_table
        )

    def display_products(self, ui, product_list, msg=None, to_append_table=False):
        products_to_show = {
            product_id: product for product_id, product in product_list.items() if product.stock_quantity > 0
        }
        if msg is None:
            ui.display_table(
                "available products", products_to_show, self.display_attributes, append_table=to_append_table
            )
        else:
            ui.display_table(msg, products_to_show, self.display_attributes, append_table=to_append_table)

    def update_product_list(self, product_list):
        updated_product_list = {
            product_id: self.update_discount(product, self.discount_percentage, self.discount_start_day)
            for product_id, product in product_list.items()
        }
        return updated_product_list

    def update_discount(self, product, discount_perc, discount_start_day):
        # Update the discount field based on current date and shelf life
        if product.expiry - datetime.utcnow().date() > timedelta(discount_start_day):
            product.discount_percentage = 0
        elif timedelta(0) < product.expiry - datetime.utcnow().date() <= timedelta(discount_start_day):
            product.discount_percentage = discount_perc
        elif product.expiry - datetime.utcnow().date() <= timedelta(0):
            product.discount_percentage = 1
        return product

    def get_selected_product(self, product_list, product_id):
        if product_id in product_list:
            self.__selected_product = product_list[product_id]

    def add_new_product_from_ui(self, repo, ui, product_list, id_generator):
        # Get new product from UI and add to repo
        self.__selected_product = self.get_new_product_from_ui(ui, id_generator)
        repo.save_product(self.__selected_product)
        if self.__selected_product.product_id not in product_list:
            product_list[self.__selected_product.product_id] = self.__selected_product
        ui.display_msg("Successfully added new product!")
        self.display_product_detail(ui)

    def get_new_product_from_ui(self, ui, id_generator):
        # logic to add new product or stock from UI
        product_type = ui.get_product_type(self.product_type_option)
        product_name = ui.get_product_name()
        package_type = ui.get_package_type(self.package_type_option)
        quantity = ui.get_package_quantity()
        price = ui.get_package_price()
        source = ui.get_product_source(self.product_source_option)
        shelf_life = ui.get_shelf_life_day()
        description = ui.get_description()
        initial_discount = 0
        creation_datetime = datetime.utcnow()
        expiry = creation_datetime.date() + timedelta(shelf_life)
        product_id = id_generator.get_product_next_id()

        if product_type == "fruit":
            product = Fruit(product_id, product_name, source, description, package_type,
                            quantity, price, initial_discount, shelf_life, creation_datetime, expiry)
        else:
            product = Vegetable(product_id, product_name, source, description, package_type,
                                quantity, price, initial_discount, shelf_life, creation_datetime, expiry)

        return product

    def search_product(self, product_list, ui):
        # Function that searches a keyword product in the product name list
        product_name = ui.get_search_name()
        p_list = dict()
        for id in product_list:
            product = product_list[id]
            if product_name.lower() in product.product_name.lower():
                p_list[product.product_id] = product

        self.display_products(ui, p_list, msg="search results")

    def edit_product_from_ui(self, repo, ui, product_list):
        # Function to edit details of a particular product
        if self.__selected_product is not None and isinstance(self.__selected_product, Product):
            editable_attributes = {
                "product_name": {
                    "Description": "Product name",
                    "UI_function": ui.get_product_name,
                    "Update_function": self.update_product_name,
                    "Type": str
                },
                "product_source": {
                    "Description": "Product source",
                    "UI_function": ui.get_product_source,
                    "UI_option": self.product_source_option,
                    "Update_function": self.update_product_source,
                    "Type": str
                },
                "description": {
                    "Description": "Description",
                    "UI_function": ui.get_description,
                    "Update_function": self.update_description,
                    "Type": str
                },
                "package_type": {
                    "Description": "Package type",
                    "UI_function": ui.get_package_type,
                    "UI_option": self.package_type_option,
                    "Update_function": self.update_package_type,
                    "Type": str
                },
                "stock_quantity": {
                    "Description": "Stock quantity",
                    "UI_function": ui.get_package_quantity,
                    "Update_function": self.update_stock_quantity,
                    "Type": float
                },
                "package_price": {
                    "Description": "Package price",
                    "UI_function": ui.get_package_price,
                    "Update_function": self.update_package_price,
                    "Type": float
                },
                "shelf_life_day": {
                    "Description": "Shelf life in day",
                    "UI_function": ui.get_shelf_life_day,
                    "Update_function": self.update_shelf_life_day,
                    "Type": int
                }
            }
            ui.display([dict({k: {"Description": v["Description"]}}) for k, v in editable_attributes.items()])

            option = ui.get_user_option()
            if option not in editable_attributes:
                raise AttributeError("Incorrect input option")

            next_step = editable_attributes[option]
            new_value_from_ui = next_step["UI_function"]() \
                if "UI_option" not in next_step \
                else next_step["UI_function"](next_step["UI_option"])

            new_value = next_step["Type"](new_value_from_ui)
            next_step["Update_function"](new_value)
            product_list[self.__selected_product.product_id] = self.__selected_product

            ui.display_msg(editable_attributes[option].get("Description") + " is updated!")
            self.display_product_detail(ui)
        else:
            raise AttributeError("No selected product")
        repo.save_product(self.__selected_product)

    def remove_product(self, repo, ui, product_list):
        # Function to delete a product from list and repository
        if self.__selected_product is not None and isinstance(self.__selected_product, Product):
            product_list[self.__selected_product.product_id].stock_quantity = 0
            repo.save_product(self.__selected_product)
        else:
            raise AttributeError("Selected product is not valid")
        ui.display_msg("Successfully deleted the product")

    def display_product_detail(self, ui):
        ui.display_table_row(self.__selected_product, self.display_attributes)

    def update_product_name(self, new_value):
        self.__selected_product.product_name = new_value

    def update_product_source(self, new_value):
        self.__selected_product.product_source = new_value

    def update_description(self, new_value):
        self.__selected_product.description = new_value

    def update_package_type(self, new_value):
        self.__selected_product.package_type = new_value

    def update_stock_quantity(self, new_value):
        self.__selected_product.stock_quantity = new_value

    def update_package_price(self, new_value):
        self.__selected_product.package_price = new_value

    def update_discount_percentage(self, new_value):
        self.__selected_product.discount_percentage = new_value

    def update_shelf_life_day(self, new_value):
        self.__selected_product.shelf_life_day = new_value
        self.__selected_product.expiry = self.__selected_product.creation_datetime.date() + timedelta(new_value)

        self.update_discount(
            self.__selected_product, self.discount_percentage, self.discount_start_day
        )
