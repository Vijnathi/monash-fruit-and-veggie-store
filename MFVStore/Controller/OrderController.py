from datetime import datetime
from functools import reduce
from MFVStore.Entities.OrderItem import OrderItem
from MFVStore.Entities.Order import Order
from MFVStore.Entities.Address import Address
from MFVStore.Entities.ShoppingCart import ShoppingCart


class OrderController:
    display_attributes = [
        "Order item ID", "Product ID", "Product name", "Package type", "Quantity", "Price"
    ]

    def __init__(self):
        self.__order_item = None

    @property
    def order_item(self):
        return self.__order_item

    @order_item.setter
    def order_item(self, order_item):
        self.__order_item = order_item

    def add_to_cart(self, customer, product, ui, id_generator, repo):
        # Add product to customer's cart
        self.create_order_item(product, ui, id_generator)
        customer.shopping_cart.ordered_item_list[self.__order_item.order_item_id] = self.__order_item
        repo.add_customer(customer)

    def create_order_item(self, product, ui, id_generator):
        # Create a new order item to be added to cart
        order_item_id = id_generator.get_order_item_next_id()
        product_id = product.product_id
        package_type = product.package_type
        quantity = int(ui.get_quantity())
        while True:
            if quantity <= product.stock_quantity:
                default_quantity = quantity
                break
            else:
                ui.display_msg("Quantity you enter should be less than stock quantity of the product")
                quantity = int(ui.get_quantity())
            # default_quantity = product.stock_quantity
        price = product.package_price * (1 - product.discount_percentage)

        self.__order_item = OrderItem(
            order_item_id, product_id, package_type, default_quantity, price
        )

    def create_order(self, ui, customer, id_generator):
        # Generate a new order for the customer
        order_id = id_generator.get_order_next_id()
        transact_time = datetime.utcnow()
        ui.display_msg("Choose an option for selecting registered address or enter new address:\n" +
                       "1: Registered Address\n2: New Address")
        option = int(input("\n"))
        if option == 1:
            delivery_address = customer.address
        else:
            delivery_address = self.get_address_from_ui(ui)

        total_amount = self.calculate_total_amount(customer.shopping_cart.ordered_item_list)
        if total_amount <= customer.balance:
            customer.balance -= total_amount
            return Order(order_id, transact_time, customer.shopping_cart.ordered_item_list, delivery_address, total_amount)

    @staticmethod
    def calculate_total_amount(ordered_item_list):
        # logic to calculate total amount of money
        # return reduce(lambda each: each.price * each.quantity, ordered_item_list.values())
        amount = 0
        for item in ordered_item_list.values():
            amount += item.price * item.quantity

        return amount

    @staticmethod
    def get_address_from_ui(ui):
        # Get new address from UI
        street_number = ui.get_street_number()
        street_address = ui.get_street()
        city = ui.get_city()
        country = ui.get_country()
        postcode = ui.get_postcode()

        address = Address(street_number, street_address, city, country, postcode)
        return address

    def view_cart(self, customer, ui, product_list=None, join_key_pos=2, extra_columns=None):
        # View all products and quantities in the cart
        ui.display_table(
            "items in shopping cart", customer.shopping_cart.ordered_item_list, self.display_attributes,
            join_table=product_list, join_key_pos=join_key_pos, extra_attribute_columns=extra_columns
        )
        total_amount = self.calculate_total_amount(customer.shopping_cart.ordered_item_list)
        ui.display_msg("\nTotal Amount: $" + str(total_amount) + "\n")

    def edit_order_item_quantity(self, order_item_id, customer, product_list, ui):
        # Edit an item's quantity in the cart

        self.__order_item = customer.shopping_cart.ordered_item_list[order_item_id]
        for product_key, product in product_list.items():
            if product_key == self.__order_item.product_id:
                product = product
                break
        quantity = int(ui.get_quantity())
        while True:
            if quantity <= product.stock_quantity:
                self.__order_item.quantity = quantity
                break
            else:
                quantity = int(ui.get_quantity())

    def check_order_items(self, customer, product_list):
        # Check if the orders can be fulfilled from stock
        order_items_list = customer.shopping_cart.ordered_item_list
        for id in order_items_list:
            order_item = order_items_list[id]
            product = product_list[order_item.product_id]
            if order_item.quantity <= product.stock_quantity:
                return None
            else:
                del customer.shopping_cart.ordered_item_list[id]
                return "Order Item of product " + product.product_name + " was deleted, as quantity was less than stock quantity"

    def reduce_stock_quantity(self, order, product_list, repo):
        # Reduce stock quantity after purchasing a product
        for id in order.ordered_item_list:
            order_item = order.ordered_item_list[id]
            product = product_list[order_item.product_id]
            product.stock_quantity -= order_item.quantity
            repo.save_product(product)

    def purchase(self, ui, customer, product_list, id_generator, repo, join_key_pos=2, extra_columns=None):
        # Checkout function when customer wants to create an order

        # Checking if shopping cart is empty
        if len(customer.shopping_cart.ordered_item_list) == 0:
            raise ValueError("No items in shopping cart! Please select products before checkout!")

        message = self.check_order_items(customer, product_list)
        if not message:
            order = self.create_order(ui, customer, id_generator)
            if order:
                self.reduce_stock_quantity(order, product_list, repo)
                customer.order_history[order.order_id] = order
                customer.shopping_cart = ShoppingCart(dict())
                repo.add_customer(customer)
                ui.display_msg(
                    "Order Id: " + str(order.order_id) + "\nTransact Time: " + str(order.transact_time) +
                    "\nDelivery Address:" + str(order.delivery_address) + "\nTotal Amount: " +
                    str(order.total_amount) + "\n"
                )
                ui.display_table(
                    "ordered items:", order.ordered_item_list, self.display_attributes,
                    join_table=product_list, join_key_pos=join_key_pos, extra_attribute_columns=extra_columns
                )
                return None
            else:
                return "balance"
        else:
            return message

    def view_orders(self, customer, ui, product_list=None, join_key_pos=2, extra_columns=None):
        # View all orders of a customer
        orders = customer.order_history
        if orders is None or len(orders) == 0:
            ui.display_msg("No orders")
        for order in orders.values():
            ui.display_msg(
                "Order Id: "+ str(order.order_id) + "\nTransact Time: " + str(order.transact_time) +
                "\nDelivery Address:" + str(order.delivery_address) + "\nTotal Amount: " +
                str(order.total_amount) + "\n"
            )

            ui.display_table("order detail", order.ordered_item_list, self.display_attributes,
                             join_table=product_list, join_key_pos=join_key_pos, extra_attribute_columns=extra_columns)
