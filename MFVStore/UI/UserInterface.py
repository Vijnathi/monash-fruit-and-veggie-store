import string
import copy

import sys
from tabulate import tabulate
import re

from MFVStore.Entities.Product import Product

class UserInterface:

    def __init__(self):
        self.__page = None
        self.__menu = None
        self.__menu_for_display = None
        self.__options = None
        self.__table = None
        self.__table_attributes = None

    @property
    def menu(self):
        return self.__menu

    @menu.setter
    def menu(self, menu):
        self.__menu = menu

    @property
    def menu_for_display(self):
        return self.__menu_for_display

    @menu_for_display.setter
    def menu_for_display(self, menu_for_display):
        self.__menu_for_display = menu_for_display

    @property
    def options(self):
        return self.__options

    @options.setter
    def options(self, options):
        self.__options = options

    @property
    def table(self):
        return self.__table

    @table.setter
    def table(self, table):
        self.__table = copy.deepcopy(table)

    @property
    def table_attributes(self):
        return self.__table_attributes

    @table_attributes.setter
    def table_attributes(self, table_attributes):
        self.__table_attributes = table_attributes

    def display_msg(self, msg):
        print(msg)

    def display(self, options=None):
        # display menu or options
        if options is None:
            # if no more item in table, then remove the option of "Input number"
            if self.__table is None or len(self.__table) == 0:
                self.__menu = [item for item in self.__menu if "Input number" not in item]

            option_list = self.gen_alphabet_range(len(self.__menu))
            self.__menu_for_display = dict(zip(option_list, self.__menu))
            menu_in_list = [
                [k, v] if v.get("Input number") is None else ["Input number", v]
                for k, v in self.__menu_for_display.items()
            ]

            print("Menu - please input option " + " or ".join([item[0] for item in menu_in_list]))
            for option in menu_in_list:
                print(str(option[0]) + " - " + str(list(option[1].values())[0].get("Description")))
        else:
            option_list = self.gen_alphabet_range(len(options))
            self.__options = dict(zip(option_list, options))
            options_in_list = [
                [k, v] for k, v in self.__options.items()
            ]

            print("Menu - please input option " + " or ".join([item[0] for item in options_in_list]))
            for option in options_in_list:
                print(str(option[0]) + " - " + str(list(option[1].values())[0].get("Description")))

    @staticmethod
    def gen_alphabet_range(number):
        return list(string.ascii_lowercase)[0:number]

    @staticmethod
    def gen_number_range(number, start_num=1):
        return range(start_num, number + 1)

    def display_table(self, table_title, table, table_attributes, append_table=False,
                      join_table=None, join_key_pos=None, extra_attribute_columns=None):

        if len(table) > 0:
            print("\n\nBelow is the list of ", table_title)

            # To print one table only
            if not append_table:
                table_for_display = []
                row_number_list = self.gen_number_range(len(table))
                temp_table = copy.deepcopy(table)
                self.__table = dict(
                    zip(
                        row_number_list,
                        [dict({"key": k, "value": v.tableau}) for k, v in temp_table.items()]
                    )
                )
                for row_number in row_number_list:
                    self.__table[row_number].get("value").insert(0, row_number_list[row_number - 1])
                    table_for_display.append(self.__table[row_number].get("value"))
            # To handle case when multiple tables need to be printed on the same page with continuous number option
            else:
                row_number_list = self.gen_number_range(len(table) + len(self.__table), start_num=len(self.__table)+1)
                temp_table = copy.deepcopy(table)
                existing_table = copy.deepcopy(self.__table)
                table_to_append = dict(
                    zip(
                        row_number_list,
                        [dict({"key": k, "value": v.tableau}) for k, v in temp_table.items()]
                    )
                )
                table_for_display = []
                for idx in range(0, len(row_number_list)):
                    row_number = row_number_list[idx]
                    table_to_append[row_number].get("value").insert(0,row_number)
                    table_for_display.append(table_to_append[row_number].get("value"))

                # merge two dictionaries
                self.__table = {**existing_table, **table_to_append}

            self.__table_attributes = copy.deepcopy(table_attributes)
            self.__table_attributes.insert(0, "Number")

            # To allow displaying extra attributes for certain object (e.g. OrderItem -> add Product Name for display)
            if join_table and join_key_pos and extra_attribute_columns:
                for idx in range(0, len(table_for_display)):
                    join_key = table_for_display[idx][join_key_pos]
                    join_table_value = join_table[join_key]
                    extra_columns = [getattr(join_table_value, attribute) for attribute in extra_attribute_columns]
                    table_for_display[idx][join_key_pos + 1: join_key_pos + 1] = list(extra_columns)

            print(tabulate(table_for_display, headers=self.__table_attributes))
            print("\n")
        else:
            print("\n\nNo", table_title, "!")
            self.__table = {}

    def display_table_row(self, row, attributes):
        print(tabulate([row.tableau], headers=attributes))

    def get_user_option(self):
        # add ui logic for get user menu option
        user_input = input("\n")

        if not self.is_input_null(user_input):
            if self.__options is None:
                if user_input.isdigit():
                    if int(user_input) in self.table:
                        return self.table[int(user_input)].get("key")
                    else:
                        self.raise_input_error()
                elif user_input.lower() in self.__menu_for_display:
                    return list(self.__menu_for_display.get(user_input.lower()).keys())[0]
                else:
                    self.raise_input_error()
            else:
                if user_input.isdigit():
                    if int(user_input) in self.table:
                        return self.table[int(user_input)].get("key")
                    else:
                        self.raise_input_error()
                elif user_input.lower() in self.options:
                    selected_option = list(self.options[user_input.lower()].keys())[0]
                    self.__options = None
                    return selected_option
                else:
                    self.raise_input_error()
        self.raise_input_error()

    def raise_input_error(self):
        self.__table = {}
        self.__options = None
        raise AttributeError("Incorrect input option")

    def get_new_value(self, option):
        # add ui logic for get new value
        return input("Input new value for " + str(option) + ": ")

    def get_user_name(self):
        # add ui logic for get user name
        counter = 4
        while counter >= 0:
            email = input("Enter your username: ")
            if not self.is_input_null(email):
                return email
            else:
                if counter==0:
                    self.raise_input_error()
                else:
                    print("Username cannot be null. Username is your email \
                        \nYou have ", counter, " more chances to enter correct username")
            counter -= 1

    def get_password_login(self):
        # add ui logic for get password
        counter = 4
        while counter >= 0:
            password = input("Enter your password: ")
            if not self.is_input_null(password):
                return password
            else:
                if counter == 0:
                    self.raise_input_error()
                else:
                    print("Password cannot be null\nYou have ", counter, " more chances to enter correct password")
            counter -= 1

    def get_product_type(self, product_option):
        # add ui logic for get product type: vege / fruit
        counter = 4
        while counter >= 0:
            print("Enter Product type")
            option_list = [dict({k: {"Description": v["Description"]}}) for k, v in product_option.items()]
            self.display(option_list)
            try:
                opt = self.get_user_option()
                return opt
            except AttributeError:
                counter -= 1
                print("Please input the correct option!\nYou have ", counter,
                      " more chances to enter correct password")
        self.raise_input_error()

    def get_product_name(self):
        # add ui logic for get name
        counter = 4
        while counter >= 0:
            product_name = input("Enter the product name: ")
            if not self.is_input_null(product_name):
                if product_name.isalpha():
                    return product_name
            counter -= 1
            print("Product name should be of alphabet format\nYou have ", counter,
                  " more chances to enter correct product name")
        raise ValueError("Incorrect input format")

    def get_package_type(self, package_option):
        counter = 4
        while counter >= 0:
            print("Enter Package type ")
            option_list = [dict({k: {"Description": v["Description"]}}) for k, v in package_option.items()]
            self.display(option_list)
            try:
                opt = self.get_user_option()
                return opt
            except AttributeError:
                counter -= 1
                print("Please input the correct option!\nYou have ", counter,
                      " more chances to enter correct password")
        self.raise_input_error()

    def get_package_quantity(self):
        counter = 4
        while counter >= 0:
            try:
                product_quantity = int(input("Enter the product quantity: "))
                return product_quantity
            except ValueError:
                print("Enter a valid product quantity")
            counter -= 1
        raise ValueError("Incorrect input format")

    def get_package_price(self):
        # add ui logic for get price
        counter = 4
        while counter >= 0:
            try:
                package_price = float(input("Enter the price per unit of the package: "))
                return package_price
            except ValueError:
                print("Enter a valid price")
            counter -= 1
        raise ValueError("Incorrect input format")

    def get_shelf_life_day(self):
        # add ui logic for get shelf life in days
        counter = 4
        while counter >= 0:
            try:
                product_shelflife = int(input("Enter the product shelf life: "))
                return product_shelflife
            except ValueError:
                print("Enter a valid price")
            counter -= 1
        raise ValueError("Incorrect input format")

    def get_product_source(self, product_source_option):
        counter = 4
        while counter >= 0:
            print("Enter product source:")
            option_list = [dict({k: {"Description": v["Description"]}}) for k, v in product_source_option.items()]
            self.display(option_list)
            try:
                opt = self.get_user_option()
                return opt
            except AttributeError:
                counter -= 1
                print("Please input the correct option!\nYou have ", counter,
                      " more chances to enter correct password")
        self.raise_input_error()

    def get_description(self):
        # add ui logic for get product description
        counter = 4
        while counter >= 0:
            product_desc = input("Enter the product Description: ")
            if not self.is_input_null(product_desc):
                    return product_desc
            else:
                print("Description cannot be null\nYou have ", counter, " more chances to enter correct Description")
            counter -= 1
        raise ValueError("Incorrect input format")

    def is_input_null(self, user_input):
        # add ui logic to validate user input not empty,
        if len(str(user_input).strip()) > 0:
            return False
        else:
            return True

    def get_last_name(self):
        counter = 4
        while counter >= 0:
            last_name = input("Enter your last name: ")
            if not self.is_input_null(last_name):
                if last_name.isalpha():
                    return last_name
                else:
                    if counter == 0:
                        raise ValueError("Incorrect input format")
                    else:
                        print("Last name can have only alpha characters\nYou have ", counter, " more chances to enter correct last name")
            else:
                if counter==0:
                    raise ValueError("Incorrect input format")
                else:
                    print("last name cannot be null\nYou have ", counter, " more chances to enter correct last name")
            counter -= 1

    def get_first_name(self):
        counter = 4
        while counter >= 0:
            first_name = input("Enter your first name: ")
            if not self.is_input_null(first_name):
                if first_name.isalpha():
                    return first_name
                else:
                    if counter == 0:
                        raise ValueError("Incorrect input format")
                    else:
                        print("First name can have only alpha characters\nYou have ", counter,
                              " more chances to enter correct first name")
            else:
                if counter == 0:
                    raise ValueError("Incorrect input format")
                else:
                    print("First name cannot be null\nYou have ", counter, " more chances to enter correct first name")
            counter -= 1

    def get_password(self):
        counter = 4
        while counter >= 0:
            password = input("Enter your password with 6 letters or numbers: ")
            if not self.is_input_null(password):
                if password.isalnum() and len(password) >= 6:
                    counter1 = 4
                    while counter1 >= 0:
                        confirm_password = input("Enter password again for confirmation: ")
                        if confirm_password and confirm_password == password:
                            return password
                        else:
                            if counter == 0:
                                self.raise_input_error()
                            else:
                                print("Enter the password correctly\nYou have ", counter1, " more chances to enter correct password")
                        counter1 -= 1
                else:
                    if counter == 0:
                        self.raise_input_error()
                    else:
                        print(
                        "Password invalid\nYou have ", counter, " more chances to enter correct password")
            else:
                if counter == 0:
                    self.raise_input_error()
                else:
                    print("Password cannot be null\nYou have ", counter, " more chances to enter correct password")
            counter -= 1

    def get_email(self):

        counter = 4
        while counter >= 0:
            email = input("Enter your email: ")
            if not self.is_input_null(email):
                if re.match("(^[a-zA-Z0-9_.]+@[a-zA-Z]+\.[a-zA-Z.]+$)", email):
                    return email
                else:
                    if counter == 0:
                        raise ValueError("Incorrect input format")
                    else:
                        print(
                        "Email should have the format - xyz@abc.com, xyz@abc.def.com \nYou have ", counter, " more chances to enter correct Email")
            else:
                if counter == 0:
                    raise ValueError("Incorrect input format")
                else:
                    print("Email cannot be null\nYou have ", counter, " more chances to enter correct Email")
            counter -= 1

    def get_street_number(self):

        counter = 4
        while counter >= 0:
            street_no = input("Enter your street number: ")
            if not self.is_input_null(street_no):
                if street_no.isdigit():
                    return street_no
                else:
                    if counter==0:
                        raise ValueError("Incorrect input format")
                    else:
                        print("Street number can have only be digits\nYou have ", counter,
                          " more chances to enter correct street number")
            else:
                if counter==0:
                    raise ValueError("Incorrect input format")
                else:
                    print("Street number cannot be null\nYou have ", counter, " more chances to enter correct street number")
            counter -= 1

    def get_street(self):
        counter = 4
        while counter >= 0:
            street = input("Enter your street and suburb(Ex- Jasper Rd, Caulfied): ")
            if not self.is_input_null(street):
                if re.match("^[a-zA-Z0-9.,-/ ]*$", street):
                    return street
                else:
                    if counter==0:
                        raise ValueError("Incorrect input format")
                    else:
                        print("Street can contain letters, digits, or one of these (. , - /) 4 characters \
                        \nYou have ", counter, " more chances to enter correct street")
            else:
                if counter==0:
                    raise ValueError("Incorrect input format")
                else:
                    print("Street cannot be null\nYou have ", counter, " more chances to enter correct street")
            counter -= 1

    def get_city(self):
        counter = 4
        while counter >= 0:
            city = input("Enter your city: ")
            if not self.is_input_null(city):
                if city.isalpha():
                    return city
                else:
                    if counter==0:
                        raise ValueError("Incorrect input format")
                    else:
                        print("City can contain only letters \
                                \nYou have ", counter, " more chances to enter correct city")
            else:
                if counter==0:
                    raise ValueError("Incorrect input format")
                else:
                    print("City cannot be null\nYou have ", counter, " more chances to enter city")
            counter -= 1

    def get_country(self):
        counter = 4
        while counter >= 0:
            country = input("Enter your country: ")
            if not self.is_input_null(country):
                if country.isalpha():
                    return country
                else:
                    if counter==0:
                        raise ValueError("Incorrect input format")
                    else:
                        print("Country can contain only letters \
                                \nYou have ", counter, " more chances to enter correct country")
            else:
                if counter==0:
                    raise ValueError("Incorrect input format")
                else:
                    print("Country cannot be null\nYou have ", counter, " more chances to enter correct country")
            counter -= 1

    def get_postcode(self):
        counter = 4
        while counter >= 0:
            postcode = input("Enter your postcode: ")
            if not self.is_input_null(postcode):
                if postcode.isdigit() and len(postcode) == 4:
                    return postcode
                else:
                    if counter==0:
                        raise ValueError("Incorrect input format")
                    else:
                        print("Postcode can contain only digits with length 4 \
                                \nYou have ", counter, " more chances to enter correct postcode")
            else:
                if counter==0:
                    raise ValueError("Incorrect input format")
                else:
                    print("Postcode cannot be null\nYou have ", counter, " more chances to enter correct postcode")
            counter -= 1

    def get_phone_number(self):
        counter = 4
        while counter >= 0:
            phone_no = input("Enter your phone number: ")
            if not self.is_input_null(phone_no):
                if phone_no.isdigit() and re.match("^[0][0-9]{9}|[9][0-9]{7}$", phone_no):
                    return phone_no
                else:
                    if counter==0:
                        raise ValueError("Incorrect input format")
                    else:
                        print("Phone number should either be landline or mobile. For landline, number should with 9 and should be 8 digits long.")
                        print("For mobile, number should start with 0 and should be 10 digits long\nYou have ", counter,
                        " more chances to enter correct phone number")
            else:
                if counter == 0:
                    raise ValueError("Incorrect input format")
                else:
                    print("Phone number cannot be null\nYou have ", counter, " more chances to enter correct Phone number")
            counter -= 1

    def get_quantity(self):

        # add ui logic for get quantity
        counter = 4
        while counter >= 0:
            quantity = input("Enter your quantity: ")
            if not self.is_input_null(quantity):
                if quantity.isdigit():
                    return quantity
                else:
                    if counter == 0:
                        raise ValueError("Incorrect input format")
                    else:
                        print("Quantity can contain only digits\
                                        \nYou have ", counter, " more chances to enter correct quantity")
            else:
                if counter == 0:
                    raise ValueError("Incorrect input format")
                else:
                    print("Quantity cannot be null\nYou have ", counter, " more chances to enter correct quantity")
            counter -= 1

    def get_amount(self):
        return 100

    def get_search_name(self):
        counter = 4
        while counter >= 0:
            key = input("Enter keyword of fruit or vegetable to search: ")
            if not self.is_input_null(key):
                if key.isalpha():
                    return key
                else:
                    if counter == 0:
                        raise ValueError("Incorrect input format")
                    else:
                        print("Key can contain only letters\
                                                \nYou have ", counter, " more chances to enter correct key")
            else:
                if counter == 0:
                    raise ValueError("Incorrect input format")
                else:
                    print("Key cannot be null\nYou have ", counter, " more chances to enter correct key")
            counter -= 1