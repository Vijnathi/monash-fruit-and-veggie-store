
class ShoppingCart:

    def __init__(self, ordered_item_list):
        self.__ordered_item_list = ordered_item_list

    def __str__(self):
        cart_str = "Ordered Item List: \n"
        # count = 1
        for key, item in self.__ordered_item_list:
            cart_str += str(key) + " - " + str(item) + "\n"
            # count += 1

        return cart_str

    @property
    def ordered_item_list(self):
        return self.__ordered_item_list

    @ordered_item_list.setter
    def ordered_item_list(self, ordered_item_list):
        self.__ordered_item_list = ordered_item_list

    def as_dict(self):
        d = dict()
        for a, v in self.ordered_item_list.items():
            if hasattr(v, "as_dict"):
                d[v.order_item_id] = v.as_dict()
        return {'ordered_item_list': d}

# s = ShoppingCart(["item1", "item2", "item3"])
# print(s)
# print(s.ordered_item_list)
# s.ordered_item_list = ["item1", "item2", "item3", "item4"]
# print(s)
# print(s.ordered_item_list)
