from MFVStore.Entities.User import User
import re
import os
from pathlib import Path
import json

class IDGenerator:

    id_mapping = {
        "C": {
            "Description": "Customer",
            "Seq_len": 4
        },
        "OW": {
            "Description": "Owner",
            "Seq_len": 4
        },
        "P": {
            "Description": "Product",
            "Seq_len": 4
        },
        "O": {
            "Description": "Order",
            "Seq_len": 4
        },
        "I": {
            "Description": "Order Item",
            "Seq_len": 4
        }
    }

    def __init__(self):
        self.__prefix = None
        self.__sequence = None
        self.__product_max_id = None
        self.__customer_max_id = None
        self.__order_max_id = None
        self.__order_item_max_id = None
        self.datadir = "Data"
        self.__id_file_name = self.get_data_directory() / "getNextID.json"
        self.initialise_nextid_file()

    def __str__(self):
        return self.__prefix + "-" + str(self.__sequence)

    @property
    def prefix(self):
        return self.__prefix

    @prefix.setter
    def prefix(self, prefix):
        self.__prefix = prefix

    @property
    def sequence(self):
        return self.__sequence

    @sequence.setter
    def sequence(self, sequence):
        self.__sequence = sequence

    @property
    def getId(self):
        return self.__prefix + "-" + str(self.__sequence)

    def get_product_next_id(self):
        # logic to get product next id

        with open(self.__id_file_name) as infile:

            data = json.load(infile)
            val = data['product']
            val += 1
            data['product'] = val

            with open(self.__id_file_name, 'w') as outfile:
                json.dump(data, outfile)
            self.__product_max_id = self.get_final_id('P', val)
            return self.__product_max_id

    def get_customer_next_id(self):
        # logic to get customer next id
        with open(self.__id_file_name) as infile:
            data = json.load(infile)
            val = data['customer']
            val += 1
            data['customer'] = val

            with open(self.__id_file_name, 'w') as outfile:
                json.dump(data, outfile)
            self.__customer_max_id = self.get_final_id('C', val)
            return self.__customer_max_id

    def get_order_item_next_id(self):
        # logic to get order item next id
        with open(self.__id_file_name) as infile:
            data = json.load(infile)
            val = data['orderitem']
            val += 1
            data['orderitem'] = val

            with open(self.__id_file_name, 'w') as outfile:
                json.dump(data, outfile)
            self.__order_item_max_id = self.get_final_id('I', val)
            return self.__order_item_max_id

    def get_order_next_id(self):
        # logic to get order next id
        with open(self.__id_file_name) as infile:
            data = json.load(infile)
            val = data['order']
            val += 1
            data['order'] = val

            with open(self.__id_file_name, 'w') as outfile:
                json.dump(data, outfile)
            self.__order_max_id = self.get_final_id('O', val)
            return self.__order_max_id

    def initialise_nextid_file(self):
        # Initialise next id file
        if not os.path.exists(self.__id_file_name):
            dictionary = dict()
            dictionary['product'] = 0
            dictionary['order'] = 0
            dictionary['customer'] = 0
            dictionary['orderitem'] = 0
            with open(self.__id_file_name, 'w') as outfile:
                json.dump(dictionary, outfile)

    def get_data_directory(self):
        current_dir = Path(os.getcwd())
        working_dir = current_dir / self.datadir
        if not os.path.exists(working_dir):
            os.makedirs(working_dir)
        return working_dir

    def get_final_id(self, prefix, value):
        # Append prefix and get final id
        if value < 10:
            append_str = '000'
        if 10 <= value < 100:
            append_str = '00'
        if 100 <= value < 1000:
            append_str = '0'

        return prefix + append_str + str(value)

    def is_id(self, user_input):
        # Validate ID function
        temp_prefix = re.findall("[a-zA-Z]+", user_input)
        temp_sequence = re.findall('\d+', user_input)

        # Check if there is only one substring being letters
        if len(temp_prefix) != 1:
            # for debug
            # print("More than one substring!")
            return False
        # Check if the prefix is at the start of string
        prefix = temp_prefix[0]
        if user_input.index(prefix) != 0:
            # for debug
            # print("Substring not a prefix!")
            return False
        # Check if the prefix is in the mapping
        if prefix not in self.id_mapping:
            # for debug
            # print("Prefix not found")
            return False

        # Check if there is only one substring being numbers
        if len(temp_sequence) != 1:
            # for debug
            # print("More than one substring for digit")
            return False
        # Check if the sequence number is more than required length
        sequence = temp_sequence[0]
        if len(sequence) != self.id_mapping[prefix]["Seq_len"]:
            # for debug
            # print("Digit length not match")
            return False

        return True

# id = IDGenerator("ABC", 1001)
# u = User(id, "Jaki", "Sam", "1234", "sam.jaki@monash.edu", 1001)
# print(u.id)
# print(u.last_name)
# print(u.first_name)
# print(u.password)
# print(u.user_id)
# print(u.fail_login_no)
#print()
#print()
#u.id = 1005
#print(u.id)
#print(u.last_name)
#print(u.first_name)
#print(u.password)
#print(u.user_id)
#print(u.fail_login_no)

# gen = IDGenerator()
# i = 0
# while i < 10:
#     print(gen.get_product_next_id())
#     print(gen.get_customer_next_id())
#     print(gen.get_order_item_next_id())
#     print(gen.get_order_next_id())
#     i+= 1

