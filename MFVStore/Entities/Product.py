
class Product:

    def __init__(self, product_id, product_name, product_source, description, package_type, stock_quantity,
                 package_price, discount_percentage, shelf_life_day, creation_datetime, expiry):
        self.__product_id = product_id
        self.__product_name = product_name
        self.__product_source = product_source
        self.__description = description
        self.__package_type = package_type
        self.__stock_quantity = stock_quantity
        self.__package_price = package_price
        self.__discount_percentage = discount_percentage
        self.__shelf_life_day = shelf_life_day
        self.__creation_datetime = creation_datetime
        self.__expiry = expiry

    @property
    def price(self):
        return self.__package_price * (1 - self.__discount_percentage)

    @property
    def tableau(self):
        a_list = []
        a_list.append(self.__product_id)
        a_list.append(self.__product_name)
        a_list.append(self.__product_source)
        a_list.append(self.__description)
        a_list.append(self.__package_type)
        a_list.append(self.__stock_quantity)
        a_list.append(self.__package_price)
        a_list.append(self.price)
        a_list.append(self.__discount_percentage)
        a_list.append(self.__expiry)
        return a_list

    @property
    def expiry(self):
        return self.__expiry

    @expiry.setter
    def expiry(self, expiry):
        self.__expiry = expiry

    @property
    def product_id(self):
        return self.__product_id

    @product_id.setter
    def product_id(self, product_id):
        self.__product_id = product_id

    @property
    def product_name(self):
        return self.__product_name

    @product_name.setter
    def product_name(self, product_name):
        self.__product_name = product_name

    @property
    def product_source(self):
        return self.__product_source

    @product_source.setter
    def product_source(self, product_source):
        self.__product_source = product_source

    @property
    def description(self):
        return self.__description

    @description.setter
    def description(self, description):
        self.__description = description

    @property
    def package_type(self):
        return self.__package_type

    @package_type.setter
    def package_type(self, package_type):
        self.__package_type = package_type

    @property
    def stock_quantity(self):
        return self.__stock_quantity

    @stock_quantity.setter
    def stock_quantity(self, stock_quantity):
        self.__stock_quantity = stock_quantity

    @property
    def package_price(self):
        return self.__package_price

    @package_price.setter
    def package_price(self, package_price):
        self.__package_price = package_price

    @property
    def discount_percentage(self):
        return self.__discount_percentage

    @discount_percentage.setter
    def discount_percentage(self, discount_percentage):
        self.__discount_percentage = discount_percentage

    @property
    def shelf_life_day(self):
        return self.__shelf_life_day

    @shelf_life_day.setter
    def shelf_life_day(self, shelf_life_day):
        self.__shelf_life_day = shelf_life_day

    @property
    def creation_datetime(self):
        return self.__creation_datetime

    @creation_datetime.setter
    def creation_datetime(self, creation_datetime):
        self.__creation_datetime = creation_datetime

    def as_dict(self):
        product_dict = dict()
        product_dict['product_id'] = self.__product_id
        product_dict['product_name'] = self.__product_name
        product_dict['product_source'] = self.__product_source
        product_dict['description'] = self.__description
        product_dict['package_type'] = self.__package_type
        product_dict['stock_quantity'] = self.__stock_quantity
        product_dict['package_price'] = self.__package_price
        product_dict['discount_percentage'] = self.__discount_percentage
        product_dict['shelf_life_day'] = self.__shelf_life_day
        product_dict['creation_datetime'] = str(self.__creation_datetime)
        product_dict['expiry'] = str(self.__expiry)
        product_dict['product_type'] = "unclassified"

        return product_dict
