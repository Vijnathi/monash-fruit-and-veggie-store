from MFVStore.Entities.Product import Product


class Vegetable(Product):

    def __init__(self, product_id, product_name, product_source, description, package_type, stock_quantity,
                 package_price, discount_percentage, shelf_life_day, creation_datetime, expiry):
        super().__init__(product_id, product_name, product_source, description, package_type, stock_quantity,
                         package_price, discount_percentage, shelf_life_day, creation_datetime, expiry)

    def __str__(self):
        return "Product ID: " + str(self.product_id) + "\nProduct Name: " + self.product_name + \
               "\nProduct Source: " + self.product_source + "\nDescription: " + self.description + \
               "\nPackage Type: " + self.package_type + "\nStock Quantity: " + str(self.stock_quantity) + \
               "\nPackage Price: " + str(self.package_price) + "\nDiscount Percentage: " + \
               str(self.discount_percentage) + "\nShelf Life: " + str(self.shelf_life_day) + \
               "\nCreation Date & Time: " + str(self.creation_datetime) + "\nExpiry: " + str(self.expiry)

    def as_dict(self):
        vege_dict = dict()
        vege_dict['product_id'] = self.product_id
        vege_dict['product_name'] = self.product_name
        vege_dict['product_source'] = self.product_source
        vege_dict['description'] = self.description
        vege_dict['package_type'] = self.package_type
        vege_dict['stock_quantity'] = self.stock_quantity
        vege_dict['package_price'] = self.package_price
        vege_dict['discount_percentage'] = self.discount_percentage
        vege_dict['shelf_life_day'] = self.shelf_life_day
        vege_dict['creation_datetime'] = str(self.creation_datetime)
        vege_dict['expiry'] = str(self.expiry)
        vege_dict['product_type'] = "vegetable"

        return vege_dict