from MFVStore.Entities.Product import Product


class Fruit(Product):

    def __init__(self, product_id, product_name, product_source, description, package_type, stock_quantity,
                 package_price, discount_percentage, shelf_life_day, creation_datetime, expiry):
        super().__init__(product_id, product_name, product_source, description, package_type, stock_quantity,
                         package_price, discount_percentage, shelf_life_day, creation_datetime, expiry)

    def __str__(self):
        return "Product ID: " + str(self.product_id) + "\nProduct Name: " + self.product_name + \
               "\nProduct Source: " + self.product_source + "\nDescription: " + self.description + \
               "\nPackage Type: " + self.package_type + "\nStock Quantity: " + str(self.stock_quantity) + \
               "\nPackage Price: " + str(self.package_price) + "\nDiscount Percentage: " + \
               str(self.discount_percentage) + "\nShelf Life: " + str(self.shelf_life_day) + \
               "\nCreation Date & Time: " + str(self.creation_datetime) + "\nExpiry: " + str(self.expiry)

    def as_dict(self):
        fruit_dict = dict()
        fruit_dict['product_id'] = self.product_id
        fruit_dict['product_name'] = self.product_name
        fruit_dict['product_source'] = self.product_source
        fruit_dict['description'] = self.description
        fruit_dict['package_type'] = self.package_type
        fruit_dict['stock_quantity'] = self.stock_quantity
        fruit_dict['package_price'] = self.package_price
        fruit_dict['discount_percentage'] = self.discount_percentage
        fruit_dict['shelf_life_day'] = self.shelf_life_day
        fruit_dict['creation_datetime'] = str(self.creation_datetime)
        fruit_dict['expiry'] = str(self.expiry)
        fruit_dict['product_type'] = "fruit"

        return fruit_dict