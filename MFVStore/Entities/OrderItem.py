class OrderItem:

    def __init__(self, order_item_id,  product_id, package_type, quantity, price):
        self.__order_item_id = order_item_id
        self.__product_id = product_id
        self.__package_type = package_type
        self.__quantity = quantity
        self.__price = price

    def __str__(self):
        return "Order Item Id: " + str(self.__order_item_id) + "\nProduct ID: " + str(self.__product_id) + "\nPackage Type: " + self.__package_type + "\nQuantity: " + str(self.__quantity) + "\nPrice: " + str(self.__price)

    @property
    def tableau(self):
        a_list = [self.__order_item_id, self.__product_id, self.__package_type, self.__quantity, self.__price]
        return a_list

    @property
    def order_item_id(self):
        return self.__order_item_id

    @order_item_id.setter
    def order_item_id(self, order_item_id):
        self.__order_item_id = order_item_id

    @property
    def product_id(self):
        return self.__product_id

    @product_id.setter
    def product_id(self, product_id):
        self.__product_id = product_id

    @property
    def package_type(self):
        return self.__package_type

    @package_type.setter
    def package_type(self, package_type):
        self.__package_type = package_type

    @property
    def quantity(self):
        return self.__quantity

    @quantity.setter
    def quantity(self, quantity):
        self.__quantity = quantity

    @property
    def price(self):
        return self.__price

    @price.setter
    def price(self, price):
        self.__price = price

    def as_dict(self):
        d = dict()
        for a, v in self.__dict__.items():
            if hasattr(v, "as_dict"):
                d[a('__')[1]] = v.as_dict()
            else:
                d[a.split('__')[1]] = v
        return d
# o = OrderItem(1,2,"Loose", 10, 5)
# print(o)
