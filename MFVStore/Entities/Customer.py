from MFVStore.Entities.Order import Order
from MFVStore.Entities.ShoppingCart import ShoppingCart
from MFVStore.Entities.User import User


class Customer(User):

    def __init__(self, id, last_name, first_name, password, fail_login_no, email,
                 address, shopping_cart, order_history, phone_number, balance):
        super().__init__(id, last_name, first_name, password, email, fail_login_no)
        self.__email = email
        self.__address = address
        self.__shopping_cart = shopping_cart
        self.__order_history = order_history
        self.__phone_number = phone_number
        self.__balance = balance

    def __str__(self):
        customer_str = "Customer ID: " + str(self.id) + "\nLast Name: " + self.last_name + "\nFirst Name: " + self.first_name + "\nUser ID: " + self.user_id + "\nEmail: " + self.__email + "\nAddress:" + str(self.__address) + "\nShopping Cart: " + str(self.__shopping_cart) + "\nOrder History: "
        count = 1
        for order in self.__order_history:
            customer_str += "\n\n" + str(count) + ") " + str(order)
            count += 1

        customer_str += "\nPhone Number: " + self.__phone_number + "\nBalance: " + str(self.__balance)

        return customer_str

    @property
    def tableau(self):
        a_list = []
        a_list.append(self.id)
        a_list.append(self.last_name)
        a_list.append(self.first_name)
        a_list.append(self.__email)
        # a_list.append(self.fail_login_no)
        a_list.append(self.__address)
        a_list.append(self.__phone_number)
        a_list.append(self.__balance)
        return a_list

    @property
    def email(self):
        return self.__email

    @email.setter
    def email(self, email):
        self.__email = email

    @property
    def address(self):
        return self.__address

    @address.setter
    def address(self, address):
        self.__address = address

    @property
    def shopping_cart(self):
        return self.__shopping_cart

    @shopping_cart.setter
    def shopping_cart(self, shopping_cart):
        self.__shopping_cart = shopping_cart

    @property
    def order_history(self):
        return self.__order_history

    @order_history.setter
    def order_history(self, order_history):
        self.__order_history = order_history

    @property
    def phone_number(self):
        return self.__phone_number

    @phone_number.setter
    def phone_number(self, phone_number):
        self.__phone_number = phone_number

    @property
    def balance(self):
        return self.__balance

    @balance.setter
    def balance(self, balance):
        self.__balance = balance


    def as_dict(self):
        d = dict()
        for a, v in self.__dict__.items():
            if hasattr(v, "as_dict"):
                d[a.split('__')[1]] = v.as_dict()
            elif isinstance(v, dict):
                temp_dict = dict()
                for k, val in v.items():
                    if hasattr(val, "as_dict"):
                        temp_dict[k] = val.as_dict()
                    else:
                        temp_dict[k] = val
                d[a.split('__')[1]] = temp_dict

            else:
                d[a.split('__')[1]] = v
        return d


# if __name__ == '__main__':
#     s = ShoppingCart(["item1", "item2", "item3"])
#     print(s)
#     o1 = Order(1, "1-2-2018", ["item1", "item2", "item3"], "VIC-3809", 34.58487, "24-9-2018")
#     o2 = Order(2, "1-2-2018", ["item4", "item5", "item6"], "VIC-3809", 34.58487, "24-9-2018")
#     o3 = Order(3, "1-2-2018", ["item7", "item8", "item9"], "VIC-3809", 34.58487, "24-9-2018")
#     c = Customer("ABC-0001", "Sam", "Tej", "pass",  0, "tej.sam@gmail.com", "VIC-3809", s, [o1,o2,o3], "0987654321", 350.00)
#     print(c)
