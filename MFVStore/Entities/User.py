class User:

    def __init__(self, id, last_name, first_name, password, user_id, fail_login_no):
        self.__id = id
        self.__last_name = last_name
        self.__first_name = first_name
        self.__password = password
        self.__user_id = user_id
        self.__fail_login_no = fail_login_no

    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self, id):
        self.__id = id

    @property
    def last_name(self):
        return self.__last_name

    @last_name.setter
    def last_name(self, last_name):
        self.__last_name = last_name

    @property
    def first_name(self):
        return self.__first_name

    @first_name.setter
    def first_name(self, first_name):
        self.__first_name = first_name

    @property
    def password(self):
        return self.__password

    @password.setter
    def password(self, password):
        self.__password = password

    @property
    def user_id(self):
        return self.__user_id

    @user_id.setter
    def user_id(self, user_id):
        self.__user_id = user_id

    @property
    def fail_login_no(self):
        return self.__fail_login_no

    @fail_login_no.setter
    def fail_login_no(self, fail_login_no):
        self.__fail_login_no = fail_login_no

