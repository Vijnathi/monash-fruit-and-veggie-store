class Address:

    def __init__(self, street_number, street_address, city, country, postcode):
        self.__street_number = street_number
        self.__street_address = street_address
        self.__city = city
        self.__country = country
        self.__postcode = postcode

    def __str__(self):
        return self.__street_number + ", " + self.__street_address + ", " + self.__city + ", " + self. __country + " - " + self.__postcode

    @property
    def street_number(self):
        return self.__street_number

    @street_number.setter
    def street_number(self, street_number):
        self.__street_number = street_number

    @property
    def street_address(self):
        return self.__street_address

    @street_address.setter
    def street_address(self, street_address):
        self.__street_address = street_address

    @property
    def city(self):
        return self.__city

    @city.setter
    def city(self, city):
        self.__city = city

    @property
    def country(self):
        return self.__country

    @country.setter
    def country(self, country):
        self.__country = country

    @property
    def postcode(self):
        return self.__postcode

    @postcode.setter
    def postcode(self, postcode):
        self.__postcode = postcode

    def as_dict(self):
        address_dict = dict()
        address_dict['street_number'] = self.__street_number
        address_dict['street_address'] = self.__street_address
        address_dict['city'] = self.__city
        address_dict['country'] = self.__country
        address_dict['postcode'] = self.__postcode

        return address_dict