from MFVStore.Entities.User import User


class Owner(User):

    def __init__(self, id, last_name, first_name, password, user_id, fail_login_no):
        super().__init__(id, last_name, first_name, password, user_id, fail_login_no)

    def __str__(self):
        return "Owner ID: " + str(self.id) + "\nLast Name: " + str(self.last_name) + \
               "\nFirst Name: " + self.first_name + "\nUser ID:" + str(self.user_id)
